<?php


return [
    'request_id_header' => (env('LOG_HELPER_REQUEST_ID_HEADER', 'X-Request-Id'))
];