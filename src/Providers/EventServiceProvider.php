<?php

namespace GetProfit\LogHelper\Providers;

use GetProfit\LogHelper\Listeners\Wildcard;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
	protected $listen = [
		'*' => [
			Wildcard::class,
		]
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();
	}
}
