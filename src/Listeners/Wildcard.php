<?php

namespace GetProfit\LogHelper\Listeners;

use GetProfit\LogHelper\Events\LoggableEventTrait;
use Illuminate\Support\Facades\Log;

class Wildcard
{
	public function handle($eventName, $eventData)
	{
		try {
			if (
				class_exists($eventName)
				&& in_array(LoggableEventTrait::class, class_uses($eventName))
			) {
				Log::info('Event: triggered', [
					'event' => $eventName,
					'data' => $eventData
				]);
			}
		}
		catch (\Throwable $e) {}
	}
}
