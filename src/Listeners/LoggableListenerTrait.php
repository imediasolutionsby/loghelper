<?php


namespace GetProfit\LogHelper\Listeners;

use Illuminate\Support\Facades\Log;

trait LoggableListenerTrait
{
	public function handle(...$args) {
		Log::info('Listener: start', [
			'listener' => get_class($this),
		]);

		$result = app()->call([$this, 'handleAction'], $args);

		Log::info('Listener: finish', [
			'listener' => get_class($this),
			'result' => $result
		]);

		return $result;
	}
}