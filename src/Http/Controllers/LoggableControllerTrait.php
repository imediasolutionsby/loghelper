<?php


namespace GetProfit\LogHelper\Http\Controllers;

use Illuminate\Support\Facades\Log;

trait LoggableControllerTrait {
	public function callAction($name, $arguments)
	{
		$bLog = property_exists($this, 'loggableMethods') && (array_key_exists($name, $this->loggableMethods) || in_array($name, $this->loggableMethods) || in_array('*', $this->loggableMethods));
		if ($bLog) {
			Log::info('Controller: start', [
				'controller' => __CLASS__,
				'action' => $this->loggableMethods[$name] ?? $name,
				'args' => $arguments
			]);
		}

		$result = parent::callAction($name, $arguments);

		if ($bLog) {
			Log::info('Controller: finish', [
				'controller' => __CLASS__,
				'action' => $this->loggableMethods[$name] ?? $name,
				'args' => $arguments
			]);
		}

		return $result;
	}
}