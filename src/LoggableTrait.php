<?php


namespace GetProfit\LogHelper;

use Illuminate\Support\Facades\Log;

trait LoggableTrait {
	public function __call($name, $arguments)
	{
		if (!method_exists($this, $name))
			trigger_error('Call to undefined method '.__CLASS__.'::'.$name.'()', E_USER_ERROR);

		if (array_key_exists($name, $this->loggableMethods)) {
			Log::info($this->loggableMethods[$name] ?? __CLASS__.'::'.$name.': start', $arguments);
		}

		$result = app()->call([$this, $name], $arguments);

		if (array_key_exists($name, $this->loggableMethods)) {
			Log::info($this->loggableMethods[$name] ?? __CLASS__.'::'.$name. ': finish');
		}

		return $result;
	}
}